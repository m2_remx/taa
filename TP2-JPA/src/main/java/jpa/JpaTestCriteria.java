package jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import domain.Department;
import domain.Employee;

/**
 * 
 * @author Callimoutou Mickaël / Fernández Armenta Mónica 
 *
 */
public class JpaTestCriteria {
	
	/**
	 * Criteria Query Test
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
		EntityManager manager = factory.createEntityManager();
		
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		 
		// Select * from employee   -------------------------------------------
		CriteriaQuery<Employee> cq_employee = cb.createQuery(Employee.class);
		Root<Employee> from = cq_employee.from(Employee.class);
		CriteriaQuery<Employee> select = cq_employee.select(from);
		TypedQuery<Employee> typedQuery = manager.createQuery(select);
		List<Employee> resultList = typedQuery.getResultList();

		for(Employee e:resultList)
			System.out.println(e);

		// Select * from departments with named query
		
		TypedQuery<Department> query = manager.createNamedQuery("Department.findAll", Department.class);
		List<Department> results = query.getResultList();
		
		for(Department d:results)
			System.out.println(d);
	}
}
