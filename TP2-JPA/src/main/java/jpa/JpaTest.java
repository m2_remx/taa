package jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import domain.Department;
import domain.Employee;

/**
 * 
 * @author Callimoutou Mickaël / Fernández Armenta Mónica 
 *
 */
public class JpaTest {

	/**
	 * JPA basic test
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
		EntityManager manager = factory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			Department department = new Department("Sales");
			Department department2 = new Department("Marketing");
			Department department3 = new Department("RRHH");
			
			manager.persist(department);
			manager.persist(department2);
			manager.persist(department3);

			Employee em = new Employee("Monik",department);
			manager.persist(em);
			
			Employee em2 = new Employee("Mickael",department2);
			manager.persist(em2);
			
			Employee em3 = new Employee("Someone",department2);
			manager.persist(em3);
			
			Employee em4 = new Employee("Someone2",department2);
			manager.persist(em4);

		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		manager.close();
		factory.close();
	}

}
