 'use strict';

angular.module('tp1taaApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-tp1taaApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-tp1taaApp-params')});
                }
                return response;
            },
        };
    });