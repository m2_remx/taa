/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.istic.m2gla.web.rest.dto;
