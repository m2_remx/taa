package com.istic.m2gla.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.istic.m2gla.domain.TODOItem;
import com.istic.m2gla.repository.TODOItemRepository;
import com.istic.m2gla.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing TODOItem.
 */
@RestController
@RequestMapping("/api")
public class TODOItemResource {

    private final Logger log = LoggerFactory.getLogger(TODOItemResource.class);

    @Inject
    private TODOItemRepository tODOItemRepository;

    /**
     * POST  /tODOItems -> Create a new tODOItem.
     */
    @RequestMapping(value = "/tODOItems",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TODOItem> createTODOItem(@RequestBody TODOItem tODOItem) throws URISyntaxException {
        log.debug("REST request to save TODOItem : {}", tODOItem);
        if (tODOItem.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new tODOItem cannot already have an ID").body(null);
        }
        TODOItem result = tODOItemRepository.save(tODOItem);
        return ResponseEntity.created(new URI("/api/tODOItems/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("tODOItem", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /tODOItems -> Updates an existing tODOItem.
     */
    @RequestMapping(value = "/tODOItems",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TODOItem> updateTODOItem(@RequestBody TODOItem tODOItem) throws URISyntaxException {
        log.debug("REST request to update TODOItem : {}", tODOItem);
        if (tODOItem.getId() == null) {
            return createTODOItem(tODOItem);
        }
        TODOItem result = tODOItemRepository.save(tODOItem);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("tODOItem", tODOItem.getId().toString()))
                .body(result);
    }

    /**
     * GET  /tODOItems -> get all the tODOItems.
     */
    @RequestMapping(value = "/tODOItems",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TODOItem> getAllTODOItems() {
        log.debug("REST request to get all TODOItems");
        return tODOItemRepository.findAll();
    }

    /**
     * GET  /tODOItems/:id -> get the "id" tODOItem.
     */
    @RequestMapping(value = "/tODOItems/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TODOItem> getTODOItem(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get TODOItem : {}", id);
        TODOItem tODOItem = tODOItemRepository.findOne(id);
        if (tODOItem == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(tODOItem, HttpStatus.OK);
    }

    /**
     * DELETE  /tODOItems/:id -> delete the "id" tODOItem.
     */
    @RequestMapping(value = "/tODOItems/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTODOItem(@PathVariable Long id) {
        log.debug("REST request to delete TODOItem : {}", id);
        tODOItemRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tODOItem", id.toString())).build();
    }
}
