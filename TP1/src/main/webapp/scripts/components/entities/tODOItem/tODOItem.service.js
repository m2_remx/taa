'use strict';

angular.module('App')
    .factory('TODOItem', function ($resource, DateUtils) {
        return $resource('api/tODOItems/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
