'use strict';

angular.module('tp1App')
    .controller('TODOItemDetailController', function ($scope, $rootScope, $stateParams, entity, TODOItem) {
        $scope.tODOItem = entity;
        $scope.load = function (id) {
            TODOItem.get({id: id}, function(result) {
                $scope.tODOItem = result;
            });
        };
        $rootScope.$on('tp1App:tODOItemUpdate', function(event, result) {
            $scope.tODOItem = result;
        });
    });
