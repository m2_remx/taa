'use strict';

angular.module('tp1App')
    .controller('TODOItemController', function ($scope, TODOItem) {
        $scope.tODOItems = [];
        $scope.loadAll = function() {
            TODOItem.query(function(result) {
               $scope.tODOItems = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TODOItem.get({id: id}, function(result) {
                $scope.tODOItem = result;
                $('#deleteTODOItemConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TODOItem.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTODOItemConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tODOItem = {content: null, endDate: null, done: null, id: null};
        };
    });
