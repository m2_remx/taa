#  TAA

In this repo you can find all the practical work for the TAA class. 

University of Rennes 1

Master 2 Computer Science - Software Engineering (GLA 2015-2016)

### ----> Project User Stories + **AngularJS**  

(Branche master) TAA/TP2-UserStories/tp2UserStories/ 

For the execution:  mvn spring-boot:run

http://localhost:8080/userstories/

![Screen Shot 2016-02-01 at 12.57.07 AM.png](https://bitbucket.org/repo/byGLxn/images/1255101629-Screen%20Shot%202016-02-01%20at%2012.57.07%20AM.png)

### ----> Project User Stories + **GWT** 

(Branche gwt) TAA/TP2-UserStories/ gwtUserStories/ 

Execute as a normal GWT project

![Screen Shot 2016-02-02 at 2.17.01 AM.png](https://bitbucket.org/repo/byGLxn/images/328913252-Screen%20Shot%202016-02-02%20at%202.17.01%20AM.png)

### Students: 
* Callimoutou Mickaël
* Fernández Armenta Mónica