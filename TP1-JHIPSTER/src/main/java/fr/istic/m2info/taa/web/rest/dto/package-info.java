/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package fr.istic.m2info.taa.web.rest.dto;
