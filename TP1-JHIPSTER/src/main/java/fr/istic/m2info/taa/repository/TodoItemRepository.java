package fr.istic.m2info.taa.repository;

import fr.istic.m2info.taa.domain.TodoItem;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TodoItem entity.
 */
public interface TodoItemRepository extends JpaRepository<TodoItem,Long> {

}
