package fr.istic.m2info.taa.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.istic.m2info.taa.domain.TodoItem;
import fr.istic.m2info.taa.repository.TodoItemRepository;
import fr.istic.m2info.taa.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing TodoItem.
 */
@RestController
@RequestMapping("/api")
public class TodoItemResource {

    private final Logger log = LoggerFactory.getLogger(TodoItemResource.class);

    @Inject
    private TodoItemRepository todoItemRepository;

    /**
     * POST  /todoItems -> Create a new todoItem.
     */
    @RequestMapping(value = "/todoItems",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoItem> createTodoItem(@RequestBody TodoItem todoItem) throws URISyntaxException {
        log.debug("REST request to save TodoItem : {}", todoItem);
        if (todoItem.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new todoItem cannot already have an ID").body(null);
        }
        TodoItem result = todoItemRepository.save(todoItem);
        return ResponseEntity.created(new URI("/api/todoItems/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("todoItem", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /todoItems -> Updates an existing todoItem.
     */
    @RequestMapping(value = "/todoItems",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoItem> updateTodoItem(@RequestBody TodoItem todoItem) throws URISyntaxException {
        log.debug("REST request to update TodoItem : {}", todoItem);
        if (todoItem.getId() == null) {
            return createTodoItem(todoItem);
        }
        TodoItem result = todoItemRepository.save(todoItem);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("todoItem", todoItem.getId().toString()))
                .body(result);
    }

    /**
     * GET  /todoItems -> get all the todoItems.
     */
    @RequestMapping(value = "/todoItems",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TodoItem> getAllTodoItems() {
        log.debug("REST request to get all TodoItems");
        return todoItemRepository.findAll();
    }

    /**
     * GET  /todoItems/:id -> get the "id" todoItem.
     */
    @RequestMapping(value = "/todoItems/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TodoItem> getTodoItem(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get TodoItem : {}", id);
        TodoItem todoItem = todoItemRepository.findOne(id);
        if (todoItem == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(todoItem, HttpStatus.OK);
    }

    /**
     * DELETE  /todoItems/:id -> delete the "id" todoItem.
     */
    @RequestMapping(value = "/todoItems/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTodoItem(@PathVariable Long id) {
        log.debug("REST request to delete TodoItem : {}", id);
        todoItemRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("todoItem", id.toString())).build();
    }
}
