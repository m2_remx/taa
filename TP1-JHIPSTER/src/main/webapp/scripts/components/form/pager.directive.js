/* globals $ */
'use strict';

angular.module('tp1App')
    .directive('tp1AppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
