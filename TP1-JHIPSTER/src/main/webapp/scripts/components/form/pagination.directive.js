/* globals $ */
'use strict';

angular.module('tp1App')
    .directive('tp1AppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
