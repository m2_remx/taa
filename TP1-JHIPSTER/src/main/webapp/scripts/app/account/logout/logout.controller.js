'use strict';

angular.module('tp1App')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
