'use strict';

angular.module('tp1App')
    .controller('TodoItemController', function ($scope, TodoItem) {
        $scope.todoItems = [];
        $scope.loadAll = function() {
            TodoItem.query(function(result) {
               $scope.todoItems = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TodoItem.get({id: id}, function(result) {
                $scope.todoItem = result;
                $('#deleteTodoItemConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TodoItem.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTodoItemConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.todoItem = {content: null, endDate: null, done: null, id: null};
        };
    });
