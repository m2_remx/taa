'use strict';

angular.module('tp1App')
    .controller('TodoItemDetailController', function ($scope, $rootScope, $stateParams, entity, TodoItem) {
        $scope.todoItem = entity;
        $scope.load = function (id) {
            TodoItem.get({id: id}, function(result) {
                $scope.todoItem = result;
            });
        };
        $rootScope.$on('tp1App:todoItemUpdate', function(event, result) {
            $scope.todoItem = result;
        });
    });
