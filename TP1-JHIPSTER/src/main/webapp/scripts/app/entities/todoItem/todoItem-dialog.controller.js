'use strict';

angular.module('tp1App').controller('TodoItemDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'TodoItem',
        function($scope, $stateParams, $modalInstance, entity, TodoItem) {

        $scope.todoItem = entity;
        $scope.load = function(id) {
            TodoItem.get({id : id}, function(result) {
                $scope.todoItem = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('tp1App:todoItemUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.todoItem.id != null) {
                TodoItem.update($scope.todoItem, onSaveFinished);
            } else {
                TodoItem.save($scope.todoItem, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
