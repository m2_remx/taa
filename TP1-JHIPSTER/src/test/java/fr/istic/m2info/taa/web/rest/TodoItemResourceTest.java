package fr.istic.m2info.taa.web.rest;

import fr.istic.m2info.taa.Application;
import fr.istic.m2info.taa.domain.TodoItem;
import fr.istic.m2info.taa.repository.TodoItemRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TodoItemResource REST controller.
 *
 * @see TodoItemResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TodoItemResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_CONTENT = "SAMPLE_TEXT";
    private static final String UPDATED_CONTENT = "UPDATED_TEXT";

    private static final DateTime DEFAULT_END_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_END_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_END_DATE_STR = dateTimeFormatter.print(DEFAULT_END_DATE);

    private static final Boolean DEFAULT_DONE = false;
    private static final Boolean UPDATED_DONE = true;

    @Inject
    private TodoItemRepository todoItemRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTodoItemMockMvc;

    private TodoItem todoItem;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TodoItemResource todoItemResource = new TodoItemResource();
        ReflectionTestUtils.setField(todoItemResource, "todoItemRepository", todoItemRepository);
        this.restTodoItemMockMvc = MockMvcBuilders.standaloneSetup(todoItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        todoItem = new TodoItem();
        todoItem.setContent(DEFAULT_CONTENT);
        todoItem.setEndDate(DEFAULT_END_DATE);
        todoItem.setDone(DEFAULT_DONE);
    }

    @Test
    @Transactional
    public void createTodoItem() throws Exception {
        int databaseSizeBeforeCreate = todoItemRepository.findAll().size();

        // Create the TodoItem

        restTodoItemMockMvc.perform(post("/api/todoItems")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(todoItem)))
                .andExpect(status().isCreated());

        // Validate the TodoItem in the database
        List<TodoItem> todoItems = todoItemRepository.findAll();
        assertThat(todoItems).hasSize(databaseSizeBeforeCreate + 1);
        TodoItem testTodoItem = todoItems.get(todoItems.size() - 1);
        assertThat(testTodoItem.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testTodoItem.getEndDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_END_DATE);
        assertThat(testTodoItem.getDone()).isEqualTo(DEFAULT_DONE);
    }

    @Test
    @Transactional
    public void getAllTodoItems() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

        // Get all the todoItems
        restTodoItemMockMvc.perform(get("/api/todoItems"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(todoItem.getId().intValue())))
                .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
                .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE_STR)))
                .andExpect(jsonPath("$.[*].done").value(hasItem(DEFAULT_DONE.booleanValue())));
    }

    @Test
    @Transactional
    public void getTodoItem() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

        // Get the todoItem
        restTodoItemMockMvc.perform(get("/api/todoItems/{id}", todoItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(todoItem.getId().intValue()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE_STR))
            .andExpect(jsonPath("$.done").value(DEFAULT_DONE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTodoItem() throws Exception {
        // Get the todoItem
        restTodoItemMockMvc.perform(get("/api/todoItems/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTodoItem() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

		int databaseSizeBeforeUpdate = todoItemRepository.findAll().size();

        // Update the todoItem
        todoItem.setContent(UPDATED_CONTENT);
        todoItem.setEndDate(UPDATED_END_DATE);
        todoItem.setDone(UPDATED_DONE);
        

        restTodoItemMockMvc.perform(put("/api/todoItems")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(todoItem)))
                .andExpect(status().isOk());

        // Validate the TodoItem in the database
        List<TodoItem> todoItems = todoItemRepository.findAll();
        assertThat(todoItems).hasSize(databaseSizeBeforeUpdate);
        TodoItem testTodoItem = todoItems.get(todoItems.size() - 1);
        assertThat(testTodoItem.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testTodoItem.getEndDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_END_DATE);
        assertThat(testTodoItem.getDone()).isEqualTo(UPDATED_DONE);
    }

    @Test
    @Transactional
    public void deleteTodoItem() throws Exception {
        // Initialize the database
        todoItemRepository.saveAndFlush(todoItem);

		int databaseSizeBeforeDelete = todoItemRepository.findAll().size();

        // Get the todoItem
        restTodoItemMockMvc.perform(delete("/api/todoItems/{id}", todoItem.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TodoItem> todoItems = todoItemRepository.findAll();
        assertThat(todoItems).hasSize(databaseSizeBeforeDelete - 1);
    }
}
