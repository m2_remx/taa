package fr.istic.m2info.taa.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import fr.istic.m2info.taa.shared.domain.Task;

import java.util.List;

/**
 * Created by fracma on 2/2/16.
 */
public interface TaskServiceAsync {
    void getTasksByUserStory(long idUserStory,AsyncCallback<List<Task>> async);

}
