package fr.istic.m2info.taa.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import fr.istic.m2info.taa.shared.domain.Epic;

import java.util.List;

/**
 * Created by fracma on 1/18/16.
 */
@RemoteServiceRelativePath("epicservice")
public interface EpicService extends RemoteService {
    /**
     * Utility/Convenience class.
     * Use EpicService.App.getInstance() to access static instance of EpicServiceAsync
     */
    public static class App {
        private static final EpicServiceAsync ourInstance = (EpicServiceAsync) GWT.create(EpicService.class);
        public static EpicServiceAsync getInstance() {
            return ourInstance;
        }
    }

    List<Epic> getAllEpic() throws IllegalArgumentException;

    void newEpic(String name, String description) throws IllegalArgumentException;
}
