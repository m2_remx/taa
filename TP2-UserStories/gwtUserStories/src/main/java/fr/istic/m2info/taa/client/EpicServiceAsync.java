package fr.istic.m2info.taa.client;


import java.util.List;
import com.google.gwt.user.client.rpc.AsyncCallback;
import fr.istic.m2info.taa.shared.domain.Epic;

/**
 * @author   Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
public interface EpicServiceAsync {

    void getAllEpic(AsyncCallback<List<Epic>> async);
    void newEpic(String name, String description,AsyncCallback async );
}
