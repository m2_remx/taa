package fr.istic.m2info.taa.shared.domain;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
public class StakeHolder implements Serializable {
	
	private Long id;
    private String name;
    
	public StakeHolder(String name) {
		super();
		this.name = name;
	}

	public StakeHolder() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
