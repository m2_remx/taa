package fr.istic.m2info.taa.server;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import fr.istic.m2info.taa.client.TaskService;
import fr.istic.m2info.taa.shared.domain.Task;
import fr.istic.m2info.taa.shared.domain.UserStory;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by fracma on 2/2/16.
 */
public class TaskServiceImpl extends RemoteServiceServlet implements TaskService {

    private ClientREST client = new ClientREST();



    @Override
    public List<Task> getTasksByUserStory(long idUserStory) throws IllegalArgumentException {
        return toList(client.getResource("http://localhost:8080/task/userstory/"+idUserStory, Task.class));
    }

    //TODO MFA Refractor toList Method
    private List<Task> toList(String json){
        System.out.println("json "+json);
        Type listType = new TypeToken<List<Task>>() {}.getType();
        List<Task> yourList = new Gson().fromJson(json, listType);
        return yourList;
    }
}