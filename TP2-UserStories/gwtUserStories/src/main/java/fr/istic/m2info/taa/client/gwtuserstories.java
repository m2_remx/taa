package fr.istic.m2info.taa.client;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;
import fr.istic.m2info.taa.shared.FieldVerifier;
import fr.istic.m2info.taa.shared.domain.Epic;
import fr.istic.m2info.taa.shared.domain.Task;
import fr.istic.m2info.taa.shared.domain.UserStory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class gwtuserstories implements EntryPoint {
    /**
     *  The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    private static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";

    /**
     * Create a remote service proxy to talk to the server-side Greeting service.
     */
    private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
    private final EpicServiceAsync epicService = GWT.create(EpicService.class);
    private final UserStoryServiceAsync userstoryService = GWT.create(UserStoryService.class);
    private final TaskServiceAsync taskService = GWT.create(TaskService.class);

    private final Messages messages = GWT.create(Messages.class);


    final ListBox epicListDown = new ListBox();
    final ListBox userStoryListDown = new ListBox();

    final Label themeUSSelected = new Label();
    final Label stakeHolderUSSelected = new Label();

    Map<Long, UserStory> userStoriesList = new HashMap<Long, UserStory>();
    //Create task list empty
      /*
      * Define a key provider for a Contact. We use the unique ID
      * as the key, which allows to maintain selection even if the
      * name changes.
      */
    ProvidesKey<Task> keyProvider = new ProvidesKey<Task>() {
        public Object getKey(Task item) {
            // Always do a null check.
            return (item == null) ? null : item.getId();
        }
    };
    // Create a CellList using the keyProvider.
    final CellList<Task> cellList = new CellList<Task>(new TaskCell(), keyProvider);

    /**
     * A custom {@link Cell} used to render a {@link Task}.
     */
    private static class TaskCell extends AbstractCell<Task> {
        @Override
        public void render(Context context, Task task, SafeHtmlBuilder safeHtmlBuilder) {
            if (task != null) {
                safeHtmlBuilder.appendEscaped(task.getName());
                safeHtmlBuilder.appendEscaped(task.getDescription());
            }
        }
    }

  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {
      //Creation left Panel

      onModuleLoadEpic();
      onModuleLoadUserStory();
      onModuleLoadDetail();
      onModuleLoadTasks();

      // Create new Horizontal panel---------
      HorizontalPanel hpr = new HorizontalPanel();
      hpr.setSpacing(100);

      // Create new Horizontal panel---------
      HorizontalPanel hp = new HorizontalPanel();
      hp.setSpacing(20);




      //hp.add(epicLabel);
      //hp.add(epicListDown);
      //hp.add(userStoryLabel);
      //hp.add(userStoryListDown);


      //-----------------------------------------

      final Button sendButton = new Button( messages.sendButton() );
    final Button epicButton = new Button( messages.epicButton() );
    final TextBox nameField = new TextBox();
    nameField.setText( messages.nameField() );
    final Label errorLabel = new Label();


    // We can add style names to widgets
    sendButton.addStyleName("sendButton");
    epicButton.addStyleName("sendButton");

    // Add the nameField and sendButton to the RootPanel
    // Use RootPanel.get() to get the entire body element
   // RootPanel.get("nameFieldContainer").add(nameField);
   // RootPanel.get("sendButtonContainer").add(sendButton);
    //RootPanel.get("errorLabelContainer").add(errorLabel);
    //RootPanel.get("epicButtonContainer").add(epicButton);

    // Focus the cursor on the name field when the app loads
    nameField.setFocus(true);
    nameField.selectAll();

    // Create the popup dialog box
    final DialogBox dialogBox = new DialogBox();
    dialogBox.setText("Remote Procedure Call");
    dialogBox.setAnimationEnabled(true);
    final Button closeButton = new Button("Close");
    // We can set the id of a widget by accessing its Element
    closeButton.getElement().setId("closeButton");
    final Label textToServerLabel = new Label();
    final HTML serverResponseLabel = new HTML();
    VerticalPanel dialogVPanel = new VerticalPanel();
    dialogVPanel.addStyleName("dialogVPanel");
    dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
    dialogVPanel.add(textToServerLabel);
    dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
    dialogVPanel.add(serverResponseLabel);
    dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
    dialogVPanel.add(closeButton);
    dialogBox.setWidget(dialogVPanel);

    // Add a handler to close the DialogBox
    closeButton.addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        dialogBox.hide();
        sendButton.setEnabled(true);
        sendButton.setFocus(true);
      }
    });

    // Create a handler for the sendButton and nameField
    class MyHandler implements ClickHandler, KeyUpHandler {
      /**
       * Fired when the user clicks on the sendButton.
       */
      public void onClick(ClickEvent event) {
        sendNameToServer();
      }

      /**
       * Fired when the user types in the nameField.
       */
      public void onKeyUp(KeyUpEvent event) {
        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
          sendNameToServer();
        }
      }

      /**
       * Send the name from the nameField to the server and wait for a response.
       */
      private void sendNameToServer() {
        // First, we validate the input.
        errorLabel.setText("");
        String textToServer = nameField.getText();
        if (!FieldVerifier.isValidName(textToServer)) {
          errorLabel.setText("Please enter at least four characters");
          return;
        }

        // Then, we send the input to the server.
        sendButton.setEnabled(false);
        textToServerLabel.setText(textToServer);
        serverResponseLabel.setText("");
        greetingService.greetServer(textToServer, new AsyncCallback<String>() {
          public void onFailure(Throwable caught) {
            // Show the RPC error message to the user
            dialogBox.setText("Remote Procedure Call - Failure");
            serverResponseLabel.addStyleName("serverResponseLabelError");
            serverResponseLabel.setHTML(SERVER_ERROR);
            dialogBox.center();
            closeButton.setFocus(true);
          }

          public void onSuccess(String result) {
            dialogBox.setText("Remote Procedure Call");
            serverResponseLabel.removeStyleName("serverResponseLabelError");
            serverResponseLabel.setHTML(result);
            dialogBox.center();
            closeButton.setFocus(true);
          }
        });
      }
    }

    //---------------------------------------------------
    // Create a handler for the epicButton
    class EpicHandler implements ClickHandler, KeyUpHandler {
      /**
       * Fired when the user clicks on the sendButton.
       */
      public void onClick(ClickEvent event) {
          getEpics();
      }

      /**
       * Fired when the user types in the nameField.
       */
      public void onKeyUp(KeyUpEvent event) {
        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
          getEpics();
        }
      }

      /**
       * Send the name from the nameField to the server and wait for a response.
       */
      private void getEpics() {
        // Then, we send the input to the server.
        epicButton.setEnabled(false);
       // RequestBuilder builder = new RequestBuilder(RequestBuilder.GET /*.POST or .PUT or .DELETE*/, someStringUrl);
       // builder.setHeader("content-type", "application/json"); /*or any other type that you're expecting*/

          epicService.getAllEpic( new AsyncCallback<List<Epic>>() {

            public void onFailure(Throwable caught) {
              System.out.println("OnFailure");
              epicButton.setEnabled(true);
            }

            public void onSuccess(List<Epic> epics) {
              System.out.println("onSuccess");
              epicButton.setEnabled(true);
              epicButton.setText("new");
            }
        });
      }
    }


      // Add a handler to send the name to the server
      MyHandler handler = new MyHandler();
      sendButton.addClickHandler(handler);
      nameField.addKeyUpHandler(handler);

      EpicHandler epichandler = new EpicHandler();
      epicButton.addClickHandler(epichandler);
      nameField.addKeyUpHandler(epichandler);




    }

    /**
     * Initialize all the UI for the Epic section
     */

    private void onModuleLoadEpic(){
        final Button deleteEpicButton = new Button( "Delete" );
        final Button editEpicButton = new Button( "Edit" );
        final Button addEpicButton = new Button( "Add" );
        final Button updateEpicButton = new Button( "Update" );

        final Label epicLabel = new Label("Epic:");

        //Epic list (all epics) by default
        epicService.getAllEpic(new AsyncCallbackAllEpic());

        //Events list
        epicListDown.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                // Get the index of the selected Item
                int indexSelec = epicListDown.getSelectedIndex();
                String idList = epicListDown.getValue(indexSelec);
                System.out.println("Search userStory by epic "+ indexSelec);
                //Search the user stories from the selected epic

                //TODO MFA Refactor
                userstoryService.getUserStoryByEpic(new Long(idList), new AsyncCallback<List<UserStory>>() {
                            public void onFailure(Throwable caught) {
                                System.out.println("OnFailure");
                            }

                            public void onSuccess(List<UserStory> userStories) {
                                System.out.println("onSuccess");
                                userStoryListDown.clear();
                                for(UserStory us: userStories){
                                    userStoryListDown.addItem(us.getName(),us.getId().toString());
                                    userStoriesList.put(us.getId(),us);
                                }

                            }
                        }
                );
            }
        });

        addEpicButton.addClickHandler(new EpicAddViewHandler());

        final Button sendNewEpicButton = Button.wrap(DOM.getElementById("sendNewEpic"));
        sendNewEpicButton.addClickHandler(new EpicAddHandler());

        HorizontalPanel hp_buttons = new HorizontalPanel();
        hp_buttons.add(addEpicButton);
        hp_buttons.add(deleteEpicButton);

        VerticalPanel vp = new VerticalPanel();
        vp.add(epicLabel);
        vp.add(epicListDown);

        RootPanel.get("listEpicWrapper").add(vp);
        RootPanel.get("bouttonsEpicWrapper").add(hp_buttons);
    }

    /**
     * Initialize all the UI for the User Story section
     */
    private void onModuleLoadUserStory(){
        final Button deleteUserStoryButton = new Button( "Delete" );
        final Button editUserStoryButton = new Button( "Edit" );
        final Button addUserStoryButton = new Button( "Add" );
        final Label userStoryLabel = new Label("User Story:");

        //User Stories List (all) by default
        userstoryService.getAllUserStory( new AsyncCallback<List<UserStory>>() {
            public void onFailure(Throwable caught) {
                System.out.println("OnFailure");
            }
            public void onSuccess(List<UserStory> userStories) {
                System.out.println("onSuccess");
                for(UserStory us: userStories)
                    userStoryListDown.addItem( us.getName(),us.getId().toString());
            }
        });


        //Events list
        userStoryListDown.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                // Get the index of the selected Item
                int indexSelec = userStoryListDown.getSelectedIndex();
                String idList = userStoryListDown.getValue(indexSelec);
                System.out.println("Search tasks by user story "+ indexSelec);

                UserStory us = userStoriesList.get(idList);
                System.out.println("US---->"+us);
                //themeUSSelected.setText(us.getTheme().getName());
                //stakeHolderUSSelected.setText(us.getStakeHolder().getName());
                //Search the user stories from the selected epic

                //TODO MFA Refactor
                taskService.getTasksByUserStory(new Long(idList), new AsyncCallback<List<Task>>() {
                            public void onFailure(Throwable caught) {
                                System.out.println("OnFailure");
                            }

                            public void onSuccess(List<Task> tasks) {
                                System.out.println("onSuccess");
                                // Push data into the CellList.
                                cellList.setRowCount(tasks.size(), true);
                                cellList.setRowData(0, tasks);
                            }
                        }
                );
            }
        });

        HorizontalPanel hp_buttons = new HorizontalPanel();
        hp_buttons.add(addUserStoryButton);
        hp_buttons.add(deleteUserStoryButton);

        VerticalPanel vp = new VerticalPanel();
        vp.add(userStoryLabel);
        vp.add(userStoryListDown);

        RootPanel.get("listUserStoryWrapper").add(vp);
        RootPanel.get("bouttonsUserStoryWrapper").add(hp_buttons);
    }

    private void onModuleLoadDetail(){
        themeUSSelected.setText("");
        stakeHolderUSSelected.setText("");
        RootPanel.get("taskDetailWrapper").add(themeUSSelected);
        RootPanel.get("taskDetailWrapper").add(stakeHolderUSSelected);
    }

    public void onModuleLoadTasks(){
        final Label tasksLabel = new Label("Tasks:");

        // Add a selection model using the same keyProvider.
        SelectionModel<Task> selectionModel= new SingleSelectionModel<Task>( keyProvider);
        cellList.setSelectionModel(selectionModel);


        VerticalPanel vp = new VerticalPanel();
        vp.add(tasksLabel);
        cellList.setStyleName("userstory-details-table");
        vp.add(cellList);

        RootPanel.get("tasksPanel").add(vp);
    }


    //-----HANDLERS
    // Create a handler for the epicAddButton
    class EpicAddViewHandler implements ClickHandler, KeyUpHandler {
        /**
         * Fired when the user clicks on the sendButton.
         */

        public void onClick(ClickEvent event) {
            RootPanel.get("addEpicWrapper").setStyleName("addEpicWrapper");
        }

        /**
         * Fired when the user types in the nameField.
         */
        public void onKeyUp(KeyUpEvent event) {
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                RootPanel.get("addEpicWrapper").setStyleName("addEpicWrapper");
            }
        }
    }

    // Create a handler for the epicAddButton
    class EpicAddHandler implements ClickHandler, KeyUpHandler {
        /**
         * Fired when the user clicks on the sendButton.
         */
        public void onClick(ClickEvent event) {
            RootPanel.get("addEpicWrapper").setStyleName("addEpicWrapperHide");


            epicService.newEpic("test", "test11", new AsyncCallback() {
                @Override
                public void onFailure(Throwable throwable) {
                    System.out.println("OnFailure");
                }

                @Override
                public void onSuccess(Object o) {
                    //Epic list (all epics) by default
                    epicService.getAllEpic(new AsyncCallbackAllEpic());
                }
            });

            //newEpic(String name, String description,AsyncCallback async )
        }

        /**
         * Fired when the user types in the nameField.
         */
        public void onKeyUp(KeyUpEvent event) {
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                RootPanel.get("addEpicWrapper").setStyleName("addEpicWrapperHide");
            }
        }
    }


    class  AsyncCallbackAllEpic implements AsyncCallback<List<Epic>> {
        public void onFailure(Throwable caught) {
            System.out.println("OnFailure");
        }
        public void onSuccess(List<Epic> epics) {
            System.out.println("onSuccess");
            for(Epic e: epics)
                epicListDown.addItem(e.getName(),e.getId().toString());
        }
    }
}
