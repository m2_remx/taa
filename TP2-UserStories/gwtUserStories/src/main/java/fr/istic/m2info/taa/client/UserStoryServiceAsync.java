package fr.istic.m2info.taa.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import fr.istic.m2info.taa.shared.domain.UserStory;

import java.util.List;

/**
 * Created by fracma on 1/20/16.
 */
public interface UserStoryServiceAsync {
    void getAllUserStory(AsyncCallback<List<UserStory>> async);
    void getUserStoryByEpic(long idEpic,AsyncCallback<List<UserStory>> async);
}
