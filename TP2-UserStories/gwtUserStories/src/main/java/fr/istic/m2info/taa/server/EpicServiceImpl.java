package fr.istic.m2info.taa.server;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import fr.istic.m2info.taa.client.EpicService;
import fr.istic.m2info.taa.shared.domain.Epic;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fracma on 1/18/16.
 */
public class EpicServiceImpl extends RemoteServiceServlet implements EpicService {

    private ClientREST client = new ClientREST();

    //TODO MFA Parametriser URL service
    public List<Epic> getAllEpic() throws IllegalArgumentException{
        System.out.println("En getAllEpic ");

        //System.out.println(client.getResource("http://localhost:8080/epic/all", Epic.class));
        //return new ArrayList<Epic>() ;
        return toList(client.getResource("http://localhost:8080/epic/all", Epic.class));
    }

    @Override
    public void newEpic(String name, String description) throws IllegalArgumentException {
        String json = "{" +
                            "name:" + name+ ","+
                            "description:"+description+
                        "}";

        client.postResource("http://localhost:8080/epic", Epic.class, json);

    }

    //TODO MFA Refractor toList Method
    private List<Epic> toList(String json){
        System.out.println("json "+json);
        Type listType = new TypeToken<List<Epic>>() {}.getType();
        List<Epic> yourList = new Gson().fromJson(json, listType);
        return yourList;
    }

}