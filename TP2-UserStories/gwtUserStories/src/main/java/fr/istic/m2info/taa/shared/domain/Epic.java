package fr.istic.m2info.taa.shared.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author   Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */

public class Epic implements Serializable{
	
	private Long id;
    private String name;
    private String description;
    private List<UserStory> stories;


	public Epic() {
	}

	/**
	 * @return the id
	 */
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the stories
	 */
	public List<UserStory> getStories() {
		return stories;
	}

	/**
	 * @param stories the stories to set
	 */
	public void setStories(List<UserStory> stories) {
		this.stories = stories;
	}

	@Override
	public String toString() {
		return "Epic{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", stories=" + stories +
				'}';
	}
}
