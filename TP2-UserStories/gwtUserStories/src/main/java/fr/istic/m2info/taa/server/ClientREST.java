package fr.istic.m2info.taa.server;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by fracma on 1/18/16.
 */
public class ClientREST {

    private DefaultHttpClient client;
    private HttpPost post;
    private HttpGet get;


    public ClientREST() {
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
        ClientConnectionManager ccm = new PoolingClientConnectionManager(registry);
        client = new DefaultHttpClient(ccm);
    }

    public String postResource(String URL, Class elementResponse, String  jsonString){
        post = new HttpPost(URL);
        post.addHeader("accept", "application/json");
        post.setHeader("Content-Type", "application/json");

       if (jsonString != null) {
            try {
                post.setEntity(new StringEntity(jsonString, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        try {

            HttpResponse response = client.execute(post);
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() >= 300) {
                System.out.println("status.getStatusCode() >= 300");
            }
            HttpEntity entity = response.getEntity();

            System.out.println("entity " + entity);
            return entity == null ? null : EntityUtils.toString(entity, "UTF-8");
        } catch (IOException e){
            e.printStackTrace();
        }
        return "";
    }


    public String getResource(String URL, Class elementResponse ){
        get = new HttpGet(URL);
        get.addHeader("accept", "application/json");
        get.setHeader("Content-Type", "application/json");

       /* if (jsonString != null) {
            try {
                post.setEntity(new StringEntity(jsonString, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }*/

        try {

            HttpResponse response = client.execute(get);
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() >= 300) {
                System.out.println("status.getStatusCode() >= 300");
            }
            HttpEntity entity = response.getEntity();

            System.out.println("entity " + entity);
            return entity == null ? null : EntityUtils.toString(entity, "UTF-8");
        } catch (IOException e){
            e.printStackTrace();
        }
        return "";
    }
}
