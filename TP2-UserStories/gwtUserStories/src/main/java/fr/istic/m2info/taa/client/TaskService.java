package fr.istic.m2info.taa.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import fr.istic.m2info.taa.shared.domain.Task;

import java.util.List;

/**
 * Created by fracma on 2/2/16.
 */
@RemoteServiceRelativePath("taskService")
public interface TaskService extends RemoteService {
    /**
     * Utility/Convenience class.
     * Use TaskService.App.getInstance() to access static instance of TaskServiceAsync
     */
    public static class App {
        private static final TaskServiceAsync ourInstance = (TaskServiceAsync) GWT.create(TaskService.class);

        public static TaskServiceAsync getInstance() {
            return ourInstance;
        }
    }

    List<Task> getTasksByUserStory(long idUserStory) throws IllegalArgumentException;;

}
