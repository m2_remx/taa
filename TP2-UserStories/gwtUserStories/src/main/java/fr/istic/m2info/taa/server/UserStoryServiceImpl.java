package fr.istic.m2info.taa.server;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import fr.istic.m2info.taa.client.UserStoryService;
import fr.istic.m2info.taa.shared.domain.Epic;
import fr.istic.m2info.taa.shared.domain.UserStory;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by fracma on 1/20/16.
 */
public class UserStoryServiceImpl extends RemoteServiceServlet implements UserStoryService {
    private ClientREST client = new ClientREST();


    @Override
    public List<UserStory> getAllUserStory() throws IllegalArgumentException {
        System.out.println("En getAllEpic ");

        //System.out.println(client.getResource("http://localhost:8080/epic/all", Epic.class));
        //return new ArrayList<Epic>() ;
        return toList(client.getResource("http://localhost:8080/userstory/all", UserStory.class));

    }

    @Override
    public List<UserStory> getUserStoryByEpic(long idEpic) throws IllegalArgumentException {
        return toList(client.getResource("http://localhost:8080/userstory/epic/"+idEpic, UserStory.class));
    }

    //TODO MFA Refractor toList Method
    private List<UserStory> toList(String json){
        System.out.println("json "+json);
        Type listType = new TypeToken<List<UserStory>>() {}.getType();
        List<UserStory> yourList = new Gson().fromJson(json, listType);
        return yourList;
    }
}