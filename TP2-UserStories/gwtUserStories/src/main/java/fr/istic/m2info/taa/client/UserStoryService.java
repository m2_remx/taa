package fr.istic.m2info.taa.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import fr.istic.m2info.taa.shared.domain.UserStory;

import java.util.List;

/**
 * Created by fracma on 1/20/16.
 */
@RemoteServiceRelativePath("userstory")
public interface UserStoryService extends RemoteService {
    /**
     * Utility/Convenience class.
     * Use UserStory.App.getInstance() to access static instance of UserStoryServiceAsync
     */
    public static class App {
        private static final UserStoryServiceAsync ourInstance = (UserStoryServiceAsync) GWT.create(UserStory.class);

        public static UserStoryServiceAsync getInstance() {
            return ourInstance;
        }
    }


    List<UserStory> getAllUserStory() throws IllegalArgumentException;;
    List<UserStory> getUserStoryByEpic(long idEpic) throws IllegalArgumentException;;

}
