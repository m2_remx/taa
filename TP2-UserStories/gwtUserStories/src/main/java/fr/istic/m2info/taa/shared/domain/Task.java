package fr.istic.m2info.taa.shared.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */


@Entity
@NamedQuery(name="findTaskByName", query=" SELECT t FROM Task t WHERE t.name LIKE :taskName ")
public class Task implements Serializable{

	private Long id;

    private String name;
    
    private String description;
      
    private UserStory story;
    
    public Task() {
		super();
	}

	public Task(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	
	
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the story
	 */
	 @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	public UserStory getStory() {
		return story;
	}

	/**
	 * @param story the story to set
	 */
	public void setStory(UserStory story) {
		this.story = story;
	}

	@Override
	public String toString() {
		return "Task{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", story=" + story +
				'}';
	}
}
