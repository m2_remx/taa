package fr.istic.m2info.taa.client;

public interface Messages extends com.google.gwt.i18n.client.Messages {
  
  @DefaultMessage("Enter your name")
  String nameField();

  @DefaultMessage("Send")
  String sendButton();

  @DefaultMessage("Epic")
  String epicButton();


}
