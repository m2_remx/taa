package fr.istic.m2info.taa.tp2UserStories;

import fr.istic.m2info.taa.tp2.dao.ManagerSingletonConnection;
import fr.istic.m2info.taa.tp2.domain.StakeHolder;
import fr.istic.m2info.taa.tp2.domain.Task;
import fr.istic.m2info.taa.tp2.domain.Theme;
import fr.istic.m2info.taa.tp2.domain.UserStory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

public class JpaTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManager manager = ManagerSingletonConnection.getManagerConnection();

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {					
			
			StakeHolder stakeHolder = new StakeHolder("Mónica");
			manager.persist(stakeHolder);
			
			Theme theme = new Theme("theme 1", "desc theme 1");
			manager.persist(theme);
			
			Task t1 = new Task ("task 1","desc task 1");
			Task t2 = new Task ("task 2","desc task 2");
			
			manager.persist(t1);
			manager.persist(t2);

			
			List<Task> tasks= new ArrayList<Task>();
			tasks.add(t1);
			tasks.add(t2);
			UserStory userStory =  new UserStory("user story 1", "desc user story 1", 5, 1,theme,tasks , stakeHolder);
			manager.persist(userStory);

		
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		ManagerSingletonConnection.closeConnection();

	}

}
