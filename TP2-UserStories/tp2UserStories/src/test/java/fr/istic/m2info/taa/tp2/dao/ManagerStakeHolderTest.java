package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.StakeHolder;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author
 */
public class ManagerStakeHolderTest {

    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();
    private ManagerStakeHolder managerStakeHolder = new ManagerStakeHolder();

    @Before
    public void setUp() throws Exception {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            StakeHolder stakeHolder = new StakeHolder("Mickaël");
            manager.persist(stakeHolder);

            StakeHolder stakeHolder2 = new StakeHolder("Monik");
            manager.persist(stakeHolder2);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
    }

    @Test
    public void testFindById() throws Exception {
        StakeHolder sh = managerStakeHolder.findById(1);
        assertNotNull(sh);
        assertEquals("Mickaël", sh.getName());
    }

    @Test
    public void testGetAll() throws Exception {
        assertEquals(2, managerStakeHolder.getAll().size());
    }

    @Test
    public void testGetByName() throws Exception {
        List<StakeHolder> sh = managerStakeHolder.getByName("Mon");
        assertEquals(1, sh.size());
        assertEquals("Monik", sh.get(0).getName());
    }

    //TODO finir
    @Test
    public void testUpdate() throws Exception {

    }

    //TODO finir
    @Test
    public void testDelete() throws Exception {

    }
}