package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.StakeHolder;
import fr.istic.m2info.taa.tp2.domain.Task;
import fr.istic.m2info.taa.tp2.domain.Theme;
import fr.istic.m2info.taa.tp2.domain.UserStory;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ManagerTaskTest{

    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();
    private UserStory userStory;
    private ManagerTask managerTask = new ManagerTask();

    @Before
    public void setUp(){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {

            StakeHolder stakeHolder = new StakeHolder("Mickaël");
            manager.persist(stakeHolder);

            Theme theme = new Theme("theme", "desc theme");
            manager.persist(theme);

            Task t1 = new Task ("task 1.1","desc task 1.1");
            Task t2 = new Task ("task 1.2","desc task 1.2");

            Task t3 = new Task ("task 3","desc task 3");


            List<Task> tasks2= new ArrayList<Task>();
            tasks2.add(t3);
            userStory =  new UserStory("user story 2", "desc user story 2", 5, 1,theme, tasks2 , stakeHolder);
            manager.persist(userStory);

            List<Task> tasks= new ArrayList<Task>();
            tasks.add(t1);
            tasks.add(t2);
            userStory =  new UserStory("user story 1", "desc user story 1", 5, 1,theme,tasks , stakeHolder);
            manager.persist(userStory);

            Assert.assertNotNull(userStory);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
    }

    @Test
    public void testFindById() throws Exception {
        Task task1 = managerTask.findById(1);
        assertEquals("task 3", task1.getName());
    }

    @Test
    public void testGetAll() throws Exception {
        assertEquals(3, managerTask.getAll().size());
    }

    @Test
    public void testGetByUserStory() throws Exception {
        List<Task> tasks = managerTask.getByUserStory(new Long("2"));

        assertEquals(2, tasks.size());
        assertEquals(new Long(2), tasks.get(0).getStory().getId());
    }

    @Test
    public void testGetByTaskName() throws Exception {
        List<Task> tasks = managerTask.getByName("task 1");
        assertEquals(2, tasks.size());
    }

    @Test
    public void testUpdate() throws Exception {
        List<Task> tasks = managerTask.getByUserStory(new Long("2"));

        assertEquals(2, tasks.size());

        Task  t2 =  tasks.get(0);
        assertEquals(new Long(2), t2.getStory().getId());

        t2.setName("name modified");
        managerTask.update(t2);


        List<Task> tasksUpd = managerTask.getByUserStory(new Long("2"));
        assertEquals(2, tasksUpd.size());
        assertEquals("name modified", tasks.get(0).getName());

    }

    @Test
    public void testDelete() throws Exception {
        List<Task> tasks = managerTask.getByUserStory(new Long("2"));
        assertEquals(2, tasks.size());
        Task  t2 =  tasks.get(0);
        managerTask.delete(t2);
        List<Task> tasksUpd = managerTask.getByUserStory(new Long("2"));
        assertEquals(0, tasksUpd.size()); //TODO changer
    }


    //@After
    /*public void close(){
        ManagerSingletonConnection.closeConnection();
    }*/


}