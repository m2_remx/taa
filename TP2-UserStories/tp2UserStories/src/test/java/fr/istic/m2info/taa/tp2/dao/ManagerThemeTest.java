package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.Theme;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author
 */
public class ManagerThemeTest {

    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();
    private ManagerTheme managerTheme = new ManagerTheme();

    @Before
    public void setUp() throws Exception {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {

            manager.persist(new Theme("theme 1", "desc theme 1"));
            manager.persist(new Theme("theme 2", "desc theme 2"));
            manager.persist(new Theme("theme 3", "desc theme 3"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
    }

    @Test
    public void testFindById() throws Exception {
        Theme theme = managerTheme.findById(1);
        assertNotNull(theme);
        assertEquals("theme 1", theme.getName());
    }

    @Test
    public void testGetAll() throws Exception {
        assertEquals(3, managerTheme.getAll().size());
    }

    @Test
    public void testGetByDescription() throws Exception {
        List<Theme> themes = managerTheme.getByDescription("desc theme");
        assertEquals(3, themes.size());
    }

    @Test
    public void testUpdate() throws Exception {
        Theme theme = managerTheme.findById(2);
        assertEquals("desc theme 2", theme.getDescription());
        theme.setDescription("Description modified");
        managerTheme.update(theme);

        theme = managerTheme.findById(2);
        assertEquals("Description modified", theme.getDescription());
    }

    @Test
    public void testDelete() throws Exception {
        Theme theme = managerTheme.findById(2);
        assertEquals("desc theme 2", theme.getDescription());

        managerTheme.delete(theme);
        theme = managerTheme.findById(2);
        assertNull(theme);

    }
}