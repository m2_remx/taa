package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.*;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author
 */
public class ManagerEpicTest{

    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();
    private ManagerEpic managerEpic = new ManagerEpic();

    List<UserStory> userStoryList;

    @Before
    public void setUp(){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {

            userStoryList = new ArrayList<UserStory>();
            StakeHolder stakeHolder = new StakeHolder("Mickaël");
            manager.persist(stakeHolder);

            Theme theme = new Theme("theme", "desc theme");
            manager.persist(theme);

            Task t1 = new Task ("task 1.1","desc task 1.1");
            Task t2 = new Task ("task 1.2","desc task 1.2");
            Task t3 = new Task ("task 3","desc task 3");


            List<Task> tasks2= new ArrayList<Task>();
            tasks2.add(t3);

            userStoryList.add(new UserStory("user story 1", "desc user story 1", 5, 1,theme, tasks2 , stakeHolder));

            List<Task> tasks= new ArrayList<Task>();
            tasks.add(t1);
            tasks.add(t2);
            userStoryList.add (new UserStory("user story 2", "desc user story 2", 5, 1,theme,tasks , stakeHolder));

            Epic epic = new Epic();
            epic.setName("Epic 1 test");
            epic.setDescription("Description Epic 1");
            epic.setStories(userStoryList);

            manager.persist(epic);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
    }

    @Test
    public void testFindById() throws Exception {
        Epic epic1 = managerEpic.findById(1);
        assertEquals("Epic 1 test", epic1.getName());
    }

    @Test
    public void testGetAll() throws Exception {
        assertEquals(1, managerEpic.getAll().size());

    }

    @Test
    public void testGetByName() throws Exception {
        List<Epic> epic = managerEpic.getByName("Epic 1");
        assertEquals(1, epic.size());
    }

    @Test
    public void testUpdate() throws Exception {
        Epic epic = managerEpic.findById(1);
        assertNotNull(epic);
        assertEquals("Description Epic 1",epic.getDescription());
        epic.setDescription("Description Modifie");

        managerEpic.update(epic);
        epic = managerEpic.findById(1);
        assertNotNull(epic);
        assertEquals("Description Modifie",epic.getDescription());
    }

    @Test
    public void testDelete() throws Exception {
        Epic epic = managerEpic.findById(1);
        assertNotNull(epic);
        managerEpic.delete(epic);

        epic = managerEpic.findById(1);
        assertNull(epic);
    }

    @Test
    public void testCreate() throws Exception {
        Epic epic = new Epic();
        epic.setName("Epic create test");
        epic.setDescription("Description Epic create");
        epic.setStories(userStoryList);

        managerEpic.create(epic);

        List<Epic> epics = managerEpic.getByName("Epic create test");
        assertEquals(1, epics.size());
        assertEquals("Description Epic create", epics.get(0).getDescription());

    }
}