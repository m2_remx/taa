package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.StakeHolder;
import fr.istic.m2info.taa.tp2.domain.Task;
import fr.istic.m2info.taa.tp2.domain.Theme;
import fr.istic.m2info.taa.tp2.domain.UserStory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author
 */
public class ManagerUserStoryTest {
    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();
    private ManagerUserStory managerUserStory = new ManagerUserStory();

    @Before
    public void setUp() throws Exception {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            UserStory userStory;
            StakeHolder stakeHolder = new StakeHolder("Mickaël");
            manager.persist(stakeHolder);

            Theme theme = new Theme("theme", "desc theme");
            manager.persist(theme);

            Task t1 = new Task ("task 1.1","desc task 1.1");
            Task t2 = new Task ("task 1.2","desc task 1.2");

            Task t3 = new Task ("task 3","desc task 3");


            List<Task> tasks2= new ArrayList<Task>();
            tasks2.add(t3);
            userStory =  new UserStory("user story 1", "desc user story 1", 5, 1,theme, tasks2 , stakeHolder);
            manager.persist(userStory);

            List<Task> tasks= new ArrayList<Task>();
            tasks.add(t1);
            tasks.add(t2);
            userStory =  new UserStory("user story 2", "desc user story 2", 5, 2,theme,tasks , stakeHolder);
            manager.persist(userStory);

            Assert.assertNotNull(userStory);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
    }

    @Test
    public void testFindById() throws Exception {
        UserStory us = managerUserStory.findById(1);
        assertNotNull(us);
        assertEquals("user story 1", us.getName());
    }

    @Test
    public void testGetAll() throws Exception {
        assertEquals(2, managerUserStory.getAll().size());
    }

    @Test
    public void testGetByPriority() throws Exception {
        List<UserStory> userStory = managerUserStory.getByPriority(2);
        assertEquals(1, userStory.size());
        assertEquals("user story 2", userStory.get(0).getName());
    }

    @Test
    public void testUpdate() throws Exception {
        UserStory us = managerUserStory.findById(1);
        assertNotNull(us);
        assertEquals("user story 1", us.getName());
        us.setName("user story 1 modified");

        managerUserStory.update(us);
        us = managerUserStory.findById(1);
        assertNotNull(us);
        assertEquals("user story 1 modified", us.getName());

    }

    @Test
    public void testDelete() throws Exception {
        UserStory us = managerUserStory.findById(1);
        assertNotNull(us);
        managerUserStory.delete(us);
        us = managerUserStory.findById(1);
        assertNull(us);
    }
}