package fr.istic.m2info.taa.tp2UserStories;

import fr.istic.m2info.taa.tp2.dao.ManagerSingletonConnection;
import fr.istic.m2info.taa.tp2.domain.StakeHolder;
import fr.istic.m2info.taa.tp2.domain.Task;
import fr.istic.m2info.taa.tp2.domain.Theme;
import fr.istic.m2info.taa.tp2.domain.UserStory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

public class DeleteTest {
    EntityManager manager = ManagerSingletonConnection.getManagerConnection();
	private UserStory userStory;
	
	@Before
    public void creationData(){

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {					
			
			StakeHolder stakeHolder = new StakeHolder("Mickaël");
			manager.persist(stakeHolder);
			
			Theme theme = new Theme("theme 2", "desc theme 2");
			manager.persist(theme);
			
			Task t1 = new Task ("task 2.1","desc task 2.1");
			Task t2 = new Task ("task 2.2","desc task 2.2");
			
			manager.persist(t1);
			manager.persist(t2);

			
			List<Task> tasks= new ArrayList<Task>();
			tasks.add(t1);
			tasks.add(t2);
			userStory =  new UserStory("user story 2 to delete", "desc user story 2", 5, 1,theme,tasks , stakeHolder);
			manager.persist(userStory);

			Assert.assertNotNull(userStory);
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		
	}
	
	@Test
	public void deleteTest(){
		boolean delete = false;
		EntityTransaction tx = manager.getTransaction();
	
		tx.begin();
		try {								
			manager.remove(userStory);
			delete= true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		Assert.assertTrue(delete);
	}
	
	@After
	public void closeAll(){
        ManagerSingletonConnection.closeConnection();
	}
}
