package fr.istic.m2info.taa.tp2.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.util.Date;

/**
 * @author   Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
@Aspect
public class LogAspect {
    //@Before("execution(public * *.*(..))")
    //@AfterReturning("execution(* *.*(..))")
    @Before("execution(* fr.istic.m2info.taa.tp2.rest.*.*(..))")
    public void log(JoinPoint pjp) throws Throwable{
        System.out.println("----------------------------------------------------------------------------");
        System.out.println("["+new Date()+"]" +" Execution of: "+pjp.getSignature().getName());
        System.out.println(pjp.getSignature().getDeclaringType());
        System.out.println("---> Arguments: ");
        for(Object o : pjp.getArgs()){
            System.out.println(o );
        }
        System.out.println("----------------------------------------------------------------------------");

    }
}
