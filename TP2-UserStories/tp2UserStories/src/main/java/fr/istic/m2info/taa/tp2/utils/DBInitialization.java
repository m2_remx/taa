package fr.istic.m2info.taa.tp2.utils;

import fr.istic.m2info.taa.tp2.dao.ManagerEpic;
import fr.istic.m2info.taa.tp2.dao.ManagerStakeHolder;
import fr.istic.m2info.taa.tp2.dao.ManagerTheme;
import fr.istic.m2info.taa.tp2.domain.*;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author
 */
public class DBInitialization {

    public static void main( String[] args ) {

        ManagerEpic managerEpic = new ManagerEpic();
        ManagerStakeHolder managerStakeHolder = new ManagerStakeHolder();
        ManagerTheme managerTheme = new ManagerTheme();

        List<UserStory> userStoryList = new ArrayList<UserStory>();


        boolean create= false;
        // Stakes Holder creation
        System.out.println("---------------------------------------------------STAKE HOLDER");

        StakeHolder sh1 = new StakeHolder("Mickael");
        create = managerStakeHolder.create(sh1);
        System.out.println("Stake Holder initialization 1 "+ create);

        StakeHolder sh2 = new StakeHolder("Monica");
        create = managerStakeHolder.create(sh2);
        System.out.println("Stake Holder initialization 1 "+create );

        System.out.println("---------------------------------------------------THEME");
        Theme theme = new Theme("Theme test", "Theme test description");
        create = managerTheme.create(theme);
        System.out.println("Theme initialization 1 " + create);


        Task t11 = new Task ("Task 1.1","Desc task 1.1");
        Task t12 = new Task ("Task 1.2","Desc task 1.2");
        Task t2 = new Task ("task 2","Desc task 2");


        List<Task> tasks2= new ArrayList<Task>();
        tasks2.add(t2);
        userStoryList.add(new UserStory("User story 1", "Desc user story Test 1", 5, 1,theme, tasks2 , sh1));

        List<Task> tasks= new ArrayList<Task>();
        tasks.add(t11);
        tasks.add(t12);
        userStoryList.add (new UserStory("user story 2", "desc user story 2", 5, 1,theme,tasks , sh2));

        Epic epic = new Epic();
        epic.setName("Epic 1 test");
        epic.setDescription("Description Epic 1");
        epic.setStories(userStoryList);

        System.out.println("---------------------------------------------------EPIC");
        managerEpic.create(epic);

        System.out.println("Initialization OK ");
    }
}
