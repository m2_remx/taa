package fr.istic.m2info.taa.tp2.rest;

import fr.istic.m2info.taa.tp2.dao.ManagerEpic;
import fr.istic.m2info.taa.tp2.dao.ManagerStakeHolder;
import fr.istic.m2info.taa.tp2.domain.Epic;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *  @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */

//@Path("/epic")
@RestController
@RequestMapping("/epic")
@Api(value="/epic", description = "Epic resource")
public class EndpointEpic {

    //private static ManagerEpic manager = new ManagerEpic();
    @Autowired
    private ManagerEpic manager;

    /*@GET
    @Path("/test")*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/test")
    public @ResponseBody String test() {
        return "{\"message\": \"Test ok\"}";
    }

    /*@GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/all")
    public @ResponseBody List<Epic> getAll() {
        System.out.println("getAll Epic ");
        List<Epic> e = manager.getAll();
        return e;
    }

    /*@GET
    @Path("/name/{name}")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/name/{name}")
    public @ResponseBody List<Epic> getEpicByName(@PathVariable("name") String name) {
        System.out.println("Get Epic by name ");
        List<Epic> e = manager.getByName(name);
        return e;
    }

    /*@GET
    @Path("/id/{id}")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/id/{id}")
    public @ResponseBody Epic getEpicById(@PathVariable("id") String id) {
        System.out.println("Get Epic by id "+id);
        Epic e = manager.findById(new Long(id));
        System.out.println(e);
        return e;
        //TODO try using lambda expression
        /*return manager.findById(new Long(id))
                .map(e -> Response.ok().entity(e).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());*/
    }


    /*@POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, value = "/update", consumes = MediaType.APPLICATION_JSON)
    public void updateEpic(@RequestBody Epic e) {
        System.out.println("updateEpic "+e);
        manager.update(e);
    }

    /*@POST
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON,  consumes = MediaType.APPLICATION_JSON)
    public void addEpic(@RequestBody Epic e) {
        System.out.println("addEpic "+e);
        manager.create(e);

    }

    /*@DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.DELETE , produces = MediaType.APPLICATION_JSON, value = "/{id}")
    public void deleteEpicById(@PathVariable("id") String id) {
        System.out.println("deleteEpicById "+id);
        manager.delete(new Long(id));
    }


}
