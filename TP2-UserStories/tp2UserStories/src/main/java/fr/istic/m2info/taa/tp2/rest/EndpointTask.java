package fr.istic.m2info.taa.tp2.rest;

import fr.istic.m2info.taa.tp2.dao.ManagerTask;
import fr.istic.m2info.taa.tp2.domain.Task;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */

//@Path("/task")
@RestController
@RequestMapping("/task")
@Api(value="/task", description = "Task resource")
public class EndpointTask {

    //private ManagerTask manager = new ManagerTask();
    @Autowired
    ManagerTask manager;


    /*@GET
    @Path("/test")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/test")
    public @ResponseBody String test() {
        return "{\"message\": \"Test ok\"}";
    }


    /*@GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/all")
    public @ResponseBody List<Task> getAll() {
        List<Task> sh = manager.getAll();
        return sh;
    }


    /*@GET
    @Path("/name/{name}")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/userstory/{id}")
    public @ResponseBody List<Task> getTaskByUserStory(@PathVariable("id") String id) {
        List<Task> sh = manager.getByUserStory(new Long(id));
        return sh;
    }

    /*@GET
    @Path("/name/{name}")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/name/{name}")
    public @ResponseBody List<Task> getTaskByName(@PathVariable("name") String name) {
        List<Task> sh = manager.getByName(name);
        return sh;
    }

    /*@GET
    @Path("/id/{id}")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/id/{id}")
    public @ResponseBody Task getTaskById(@PathVariable("id") String id) {
        Task task = manager.findById(new Long(id));
        System.out.println(task);
        return task;
    }

    /*@POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, value = "/update", consumes = MediaType.APPLICATION_JSON)
    public void updateTask(@RequestBody  Task task) {
        System.out.println("update Task "+task);
        manager.update(task);
    }

    /*@POST
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public void addTask(@RequestBody  Task task) {
        System.out.println("Add Task "+task);
        manager.create(task);

    }

    /*@DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.DELETE , produces = MediaType.APPLICATION_JSON, value = "/{id}")
    public Response deleteTaskById(@PathVariable("id") String id) {
        if(!manager.delete(new Long(id)))
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.status(Response.Status.OK).build();
    }
}
