package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.Epic;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 *
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
public class ManagerEpic{

	private EntityManager manager = ManagerSingletonConnection.getManagerConnection();;

    /**
     *
     * @param id
     * @return
     */
	public Epic findById(long id) {
		Epic epic = manager.find(Epic.class, id);
        return epic;
	}

    /**
     *
     * @return
     */
	public List<Epic> getAll() {
		CriteriaBuilder cb = manager.getCriteriaBuilder();

		CriteriaQuery<Epic> q = cb.createQuery(Epic.class);
		Root<Epic> c = q.from(Epic.class);
		q.select(c);
		TypedQuery<Epic> query = manager.createQuery(q);
		List<Epic> result = query.getResultList();

		return result;
	}


	/**
	 *
	 * @param epicName
	 * @return
	 */
	public List<Epic> getByName(String epicName){
		TypedQuery<Epic> t = manager.createNamedQuery("findEpicByName", Epic.class);
		t.setParameter("epicName", epicName+"%");
		List<Epic> epicList = t.getResultList();
		return epicList;
	}

	/**
	 *
	 * @param epic
	 * @return
	 */
	public boolean update(Epic epic){
		try {
			manager.getTransaction().begin();
			manager.getTransaction().commit();
		}catch(Exception e){
			System.out.println("update Epic " + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 *
	 * @param epic
	 * @return
	 */
	public boolean delete(Epic epic){
		try {
			EntityTransaction et = manager.getTransaction();
			et.begin();
			manager.remove(epic);
			et.commit();
			return true;
		} catch (Exception e) {
			System.out.println("Delete epic " + e.getMessage());
		}
		return false;
	}


	public boolean delete(long id){
		try {
			Epic epic = findById(id);
			if(epic!=null){
				return delete(epic);
			}
		} catch (Exception e) {
			System.out.println("Delete epic " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}


	/**
	 *
	 * @param epic
	 * @return
	 */
	public boolean create(Epic epic){
		try {
			EntityTransaction et = manager.getTransaction();
			et.begin();
			manager.persist(epic);
			et.commit();
			return true;
		} catch (Exception e) {
			System.out.println("create epic " + e.getMessage());
		}
		return false;
	}
}
