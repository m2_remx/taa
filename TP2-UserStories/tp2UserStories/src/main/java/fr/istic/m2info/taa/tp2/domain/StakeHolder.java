package fr.istic.m2info.taa.tp2.domain;

import javax.persistence.*;


/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
@Entity
@NamedQuery(name="findStakeHolderByName", query="SELECT sh FROM StakeHolder sh WHERE sh.name = :name ")
public class StakeHolder {
	
	private Long id;

    private String name;
    
	public StakeHolder(String name) {
		super();
		this.name = name;
	}

	public StakeHolder() {
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
