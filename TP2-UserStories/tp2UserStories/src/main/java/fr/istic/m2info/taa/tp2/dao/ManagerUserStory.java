package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.Epic;
import fr.istic.m2info.taa.tp2.domain.StakeHolder;
import fr.istic.m2info.taa.tp2.domain.UserStory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */
public class ManagerUserStory {
    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();;

    @Autowired
    private ManagerStakeHolder managerStakeHolder;

    @Autowired
    ManagerEpic managerEpic;
    /**
     *
     * @param id
     * @return
     */
    public UserStory findById(long id) {
        UserStory userStory = manager.find(UserStory.class, id);
        return userStory;
    }

    /**
     *
     * @return
     */
    public List<UserStory> getAll() {
        CriteriaBuilder cb = manager.getCriteriaBuilder();

        CriteriaQuery<UserStory> q = cb.createQuery(UserStory.class);
        Root<UserStory> c = q.from(UserStory.class);
        q.select(c);
        TypedQuery<UserStory> query = manager.createQuery(q);
        List<UserStory> result = query.getResultList();

        return result;
    }

    /**
     *
     * @param priority
     * @return
     */
    public List<UserStory> getByPriority(int priority){
        TypedQuery<UserStory> t = manager.createNamedQuery("findUserStoryByPriority", UserStory.class);
        t.setParameter("priority", priority);
        List<UserStory> userStories = t.getResultList();
        return userStories;
    }

    /**
     *
     * @param epicId
     * @return
     */
    public List<UserStory> getByEpicId(long epicId){
        TypedQuery<UserStory> t = manager.createNamedQuery("findUserStoryByEpic", UserStory.class);
        System.out.println("Searching epic... "+ epicId);
        Epic e =managerEpic.findById(epicId);
        if(e != null){
            t.setParameter("epic", e);
            List<UserStory> userStories = t.getResultList();
            return userStories;
        }

        return new ArrayList<UserStory>();
    }


    /**
     *
     * @param userStory
     * @return
     */
    public boolean update(UserStory userStory){
        try {
            manager.getTransaction().begin();
            manager.getTransaction().commit();
        }catch(Exception e){
            System.out.println("update UserStory " + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     *
     * @param userStory
     * @return
     */
    public boolean delete(UserStory userStory){
        try {
            EntityTransaction et = manager.getTransaction();
            et.begin();
            manager.remove(userStory);
            et.commit();
            return true;
        } catch (Exception e) {
            System.out.println("Delete UserStory " + e.getMessage());
            e.printStackTrace();
        }
       return false;
    }


    /**
     *
     * @param id
     * @return
     */
    public boolean delete(Long id){
        try {
            UserStory userStory = findById(id);
            if(userStory!= null)
                return delete(userStory);
        } catch (Exception e) {
            System.out.println("Delete UserStory " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }


    /**
     *
     * @param userStory
     * @return
     */
    public boolean create(UserStory userStory){
        StakeHolder stackeHolder = null;
        try {
            EntityTransaction et = manager.getTransaction();
            et.begin();

            //We search if there is a StakeHolder with the same name, if not, we create it
            if(userStory.getStakeHolder()!= null){
                if(userStory.getStakeHolder().getId()!= null  && userStory.getStakeHolder().getId()==0 && userStory.getStakeHolder().getName().length()>0){
                    List<StakeHolder> stakeHolderList = managerStakeHolder.getByName(userStory.getStakeHolder().getName());
                    if(stakeHolderList.size()>0){
                        stackeHolder = stakeHolderList.get(0);
                    }else{
                        stackeHolder = new StakeHolder(userStory.getStakeHolder().getName());
                        manager.persist(stackeHolder);
                    }
                }else{
                    stackeHolder = new StakeHolder(userStory.getStakeHolder().getName());
                    manager.persist(stackeHolder);
                }
                userStory.setStakeHolder(stackeHolder);
            }
            manager.persist(userStory);
            et.commit();
            return true;
        } catch (Exception e) {
            System.out.println("create UserStory " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
}
