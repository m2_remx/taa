package fr.istic.m2info.taa.tp2.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author   Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
//@XmlRootElement(name = "epic")
@Entity
@NamedQuery(name="findEpicByName", query="SELECT e FROM Epic e WHERE e.name LIKE :epicName ")
public class Epic {
	
	private Long id;

    private String name;
    
    private String description;
       
    private List<UserStory> stories;



    /**
	 * @return the id
	 */
    @Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the stories
	 */
	@OneToMany(mappedBy="epic")
	public List<UserStory> getStories() {
		return stories;
	}

	/**
	 * @param stories the stories to set
	 */
	public void setStories(List<UserStory> stories) {
		this.stories = stories;
	}

	@Override
	public String toString() {
		return "Epic{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", stories=" + stories +
				'}';
	}
}
