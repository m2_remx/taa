package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.Theme;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author
 */
public class ManagerTheme {
    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();;

    /**
     *
     * @param id
     * @return
     */
    public Theme findById(long id) {
        Theme theme = manager.find(Theme.class, id);
        return theme;
    }

    /***
     *
     * @return
     */
    public List<Theme> getAll() {
        CriteriaBuilder cb = manager.getCriteriaBuilder();

        CriteriaQuery<Theme> q = cb.createQuery(Theme.class);
        Root<Theme> c = q.from(Theme.class);
        q.select(c);
        TypedQuery<Theme> query = manager.createQuery(q);
        List<Theme> result = query.getResultList();

        return result;
    }

    /**
     *
     * @param description
     * @return
     */
    public List<Theme> getByDescription(String description){
        TypedQuery<Theme> t = manager.createNamedQuery("findThemeByDescription", Theme.class);
        t.setParameter("description", description+"%");
        List<Theme> themeList = t.getResultList();
        return themeList;
    }

    /**
     *
     * @param theme
     * @return
     */
    public boolean update(Theme theme){
        try {
            manager.getTransaction().begin();
            manager.getTransaction().commit();
        }catch(Exception e){
            System.out.println("update Theme " + e.getMessage());
            return false;
        }
        return true;
    }


    /**
     *
     * @param theme
     * @return
     */
    public boolean delete(Theme theme){
        boolean delete = false;
        manager.getTransaction().begin();
        try {
            manager.remove(theme);
            delete= true;
        } catch (Exception e) {
            System.out.println("Delete Theme " + e.getMessage());
        }
        manager.getTransaction().commit();
        return delete;
    }

    /**
     *
     * @param id
     * @return
     */
    public boolean delete(Long id){
        boolean delete = false;
        Theme theme = findById(id);
        manager.getTransaction().begin();
        try {
            manager.remove(theme);
            delete= true;
        } catch (Exception e) {
            System.out.println("Delete Theme " + e.getMessage());
        }
        manager.getTransaction().commit();
        return delete;
    }

    /**
     *
     * @param theme
     * @return
     */
    public boolean create(Theme theme){
        boolean create = false;
        manager.getTransaction().begin();
        try {
            manager.persist(theme);
            create= true;
        } catch (Exception e) {
            System.out.println("Delete Theme " + e.getMessage());
        }
        manager.getTransaction().commit();
        return create;
    }

}
