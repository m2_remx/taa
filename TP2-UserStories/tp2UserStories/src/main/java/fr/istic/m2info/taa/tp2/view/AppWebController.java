package fr.istic.m2info.taa.tp2.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by fracma on 1/17/16.
 */

@Controller
@RequestMapping("/userstories")
public class AppWebController {

    @RequestMapping("/")
    public ModelAndView index(){
        System.out.print("En index.....");
        return new ModelAndView("index");
    }

}
