package fr.istic.m2info.taa.tp2.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
@Entity
@NamedQuery(name="findThemeByDescription", query="SELECT t FROM Theme t WHERE t.description LIKE :description ")
public class Theme {
	
	private Long id;

    private String name;
    
    private String description;
       
    private List<UserStory> stories;
        
    public Theme(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public Theme() {
	}

	/**
	 * @return the id
	 */
    @Id
    @GeneratedValue
	public Long getId() {
		return id;
	}

    public void setId(Long id) {
		this.id = id;
	}
    
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the stories
	 */
	 @OneToMany(mappedBy="theme")
	 @JsonIgnore
	public List<UserStory> getStories() {
		return stories;
	}

	/**
	 * @param stories the stories to set
	 */
	public void setStories(List<UserStory> stories) {
		this.stories = stories;
	}
}
