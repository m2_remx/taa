package fr.istic.m2info.taa.tp2.rest;

import fr.istic.m2info.taa.tp2.dao.ManagerStakeHolder;
import fr.istic.m2info.taa.tp2.domain.StakeHolder;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */

@RestController
@RequestMapping("/stakeholder")
//@Path("/stakeholder")
@Api(value="/stakeholder", description = "Stake Holder resource")
public class EndpointStakeHolder {

    //private static ManagerStakeHolder manager = new ManagerStakeHolder ();

    @Autowired
    private ManagerStakeHolder manager;

    public EndpointStakeHolder(ManagerStakeHolder manager) {
        this.manager = manager;
    }

    public EndpointStakeHolder() {
    }


    /*@GET
    @Path("/test")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/test")
    public @ResponseBody String test() {
        return "{\"message\": \"Test ok\"}";
    }

    /*@GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/all")
    public @ResponseBody List<StakeHolder> getAll() {
        List<StakeHolder> sh = manager.getAll();
        return sh;
    }

    /*@GET
    @Path("/name/{name}")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/name/{name}")
    public @ResponseBody List<StakeHolder> getStakeHolderByName(@PathVariable("name") String name) {
        List<StakeHolder> sh = manager.getByName(name);
        return sh;
    }

   /* @GET
    @Path("/{id}")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/{id}")
    public @ResponseBody StakeHolder getStakeHolderById(@PathVariable("id") String id) {
        StakeHolder sh = manager.findById(new Long(id));
        System.out.println(sh);
        return sh;
    }

    /*@POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, value = "/update")
    public void updateStakeHolder(@RequestBody StakeHolder e) {
        manager.update(e);
    }

    /*@POST
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON)
    public void addStakeHolder(@RequestBody StakeHolder sh) {
        manager.create(sh);
    }

    /*@DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.DELETE , produces = MediaType.APPLICATION_JSON, value = "/{id}")
    public Response deleteEpicById(@PathVariable("id") String id) {
        if(!manager.delete(new Long(id)))
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.status(Response.Status.OK).build();

    }
}
