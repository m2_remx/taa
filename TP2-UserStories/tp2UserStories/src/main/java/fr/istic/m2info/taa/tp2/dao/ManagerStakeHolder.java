package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.StakeHolder;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */
public class ManagerStakeHolder {

    private EntityManager manager = ManagerSingletonConnection.getManagerConnection();;

    /**
     *
     * @param id
     * @return
     */
    public StakeHolder findById(long id) {
        StakeHolder stakeHolder = manager.find(StakeHolder.class, id);
        return stakeHolder;
    }

    /**
     *
     * @return
     */
    public List<StakeHolder> getAll() {
        CriteriaBuilder cb = manager.getCriteriaBuilder();

        CriteriaQuery<StakeHolder> q = cb.createQuery(StakeHolder.class);
        Root<StakeHolder> c = q.from(StakeHolder.class);
        q.select(c);
        TypedQuery<StakeHolder> query = manager.createQuery(q);
        List<StakeHolder> result = query.getResultList();

        return result;
    }

    /**
     *
     * @param shName
     * @return
     */
    public List<StakeHolder> getByName(String shName){
        TypedQuery<StakeHolder> t = manager.createNamedQuery("findStakeHolderByName", StakeHolder.class);
        t.setParameter("name", shName+"%");
        List<StakeHolder> shList = t.getResultList();
        return shList;
    }

    /**
     *
     * @param sh
     * @return
     */
    public boolean update(StakeHolder sh){
        try {
            manager.getTransaction().begin();
            manager.getTransaction().commit();
        }catch(Exception e){
            System.out.println("update StakeHolder " + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     *
     * @param id
     * @return
     */
    public boolean delete(long id){
        boolean delete = false;
        StakeHolder sh = findById(id);

        manager.getTransaction().begin();
        try {
            manager.remove(sh);
            delete= true;
        } catch (Exception e) {
            System.out.println("Delete StakeHolder by id " + e.getMessage());
        }
        manager.getTransaction().commit();
        return delete;
    }

    /**
     *
     * @param sh
     * @return
     */
    public boolean delete(StakeHolder sh){
        boolean delete = false;
        manager.getTransaction().begin();
        try {
            manager.remove(sh);
            delete= true;
        } catch (Exception e) {
            System.out.println("Delete StakeHolder " + e.getMessage());
        }
        manager.getTransaction().commit();
        return delete;
    }

    /**
     *
     * @param sh
     * @return
     */
    public boolean create(StakeHolder sh){
        boolean create = false;
        manager.getTransaction().begin();
        try {
            manager.persist(sh);
            create= true;
        } catch (Exception e) {
            System.out.println("create StakeHolder " + e.getMessage());
        }
        manager.getTransaction().commit();
        return create;
    }
}
