package fr.istic.m2info.taa.tp2.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
//@ImportResource("client-beans.xml")
//@EnableWebMvc
//@ComponentScan(basePackages = { "fr.istic.m2info.taa.tp2.rest" ,"fr.istic.m2info.taa.tp2.aspect", "fr.istic.m2info.taa.tp2.view"})
public class SpringApplicationTest {
    public static void main(String[] args) throws Exception {
        ApplicationContext ctx =  SpringApplication.run(SpringApplicationTest.class, args);

       // ApplicationContext context = SpringApplication.run( [SpringServlet.class, "classpath:/client-beans.xml"] as Object [], args)
       /* String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }*/

    }

}
