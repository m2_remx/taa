package fr.istic.m2info.taa.tp2.rest;

import fr.istic.m2info.taa.tp2.dao.ManagerEpic;
import fr.istic.m2info.taa.tp2.dao.ManagerUserStory;
import fr.istic.m2info.taa.tp2.domain.UserStory;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */

//@Path("/userstory")
@RestController
@RequestMapping("/userstory")
@Api(value="/userstory", description = "User Story resource")
public class EndpointUserStory {

    private static final Logger logger = Logger.getLogger(EndpointEpic.class.getName());
    //private static ManagerUserStory manager = new ManagerUserStory ();
    @Autowired
    private ManagerUserStory manager;

    /*@GET
    @Path("/test")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/test")
    private @ResponseBody String test() {
        return "{\"message\": \"Test ok\"}";
    }

    /*@GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/all")
    public @ResponseBody List<UserStory> getAll() {
        List<UserStory> us = manager.getAll();
        return us;
    }

   /* @GET
    @Path("/name/{priority}")
    @Produces(MediaType.APPLICATION_JSON)*/
   @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/priority/{priority}")
   public @ResponseBody List<UserStory> getByPriority(@PathVariable("priority") String priority) {
        List<UserStory> us = manager.getByPriority(new Integer(priority));
        return us;
    }

    /* @GET
    @Path("/name/{priority}")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/epic/{epicId}")
    public @ResponseBody List<UserStory> getByEpic(@PathVariable("epicId") String epicId) {
        List<UserStory> us = manager.getByEpicId(new Long(epicId));
        return us;
    }

    /*@GET
    @Path("/id/{id}")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/id/{id}")
    public @ResponseBody UserStory getUserStoryById(@PathVariable("id") String id) {
        System.out.println("Get UserStory by id "+id);
        UserStory userStory = manager.findById(new Long(id));
        System.out.println(userStory);
        return userStory;
    }

    /*@POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, value = "/update", consumes = MediaType.APPLICATION_JSON)
    public void updateUserStory(@RequestBody UserStory userStory) {
        System.out.println("Update UserStory "+userStory);
        manager.update(userStory);
    }

    /*@POST
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public void addUserStory(@RequestBody UserStory userStory) {
        manager.create(userStory);
    }

    /*@DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.DELETE , produces = MediaType.APPLICATION_JSON, value = "/{id}")
    public Response deleteUserStoryById(@PathVariable("id") String id) {
        if(!manager.delete(new Long(id)))
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.status(Response.Status.OK).build();
    }
}
