package fr.istic.m2info.taa.tp2.rest;

import fr.istic.m2info.taa.tp2.dao.ManagerTheme;
import fr.istic.m2info.taa.tp2.domain.Theme;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */

//@Path("/theme")
@RestController
@RequestMapping("/theme")
@Api(value="/theme", description = "Theme resource")
public class EndpointTheme {

    //private static ManagerTheme manager = new ManagerTheme ();

    @Autowired
    private ManagerTheme manager;


    /*@GET
    @Path("/test")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/test")
    private @ResponseBody String test() {
        return "{\"message\": \"Test ok\"}";
    }

    /*@GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/all")
    public @ResponseBody List<Theme> getAll() {
        List<Theme> sh = manager.getAll();
        return sh;
    }


    /*@GET
    @Path("/name/{name}")
    @Produces(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/name/{name}")
    public @ResponseBody List<Theme> getThemeByName(@PathVariable("name") String description) {
        List<Theme> sh = manager.getByDescription(description);
        return sh;
    }

    /*@GET
    @Path("/id/{id}")
    @Produces(value = MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON, value = "/id/{id}")
    public @ResponseBody Theme getThemeById(@PathVariable("id") String id) {
        Theme theme = manager.findById(new Long(id));
        System.out.println(theme);
        return theme;
    }

    /*@POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, value = "/update", consumes = MediaType.APPLICATION_JSON)
    public void updateTheme(Theme task) {
        System.out.println("Update Theme "+task);
        manager.update(task);
    }

    /*@POST
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public void addTheme(Theme task) {
        System.out.println("add Theme "+task);
        manager.create(task);
    }

    /*@DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)*/
    @RequestMapping(method = RequestMethod.DELETE , produces = MediaType.APPLICATION_JSON, value = "/{id}")
    public Response deleteThemeById(@PathVariable("id") String id) {
        if(!manager.delete(new Long(id)))
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.status(Response.Status.OK).build();
    }
}
