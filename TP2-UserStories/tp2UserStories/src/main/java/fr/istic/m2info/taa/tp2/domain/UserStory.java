package fr.istic.m2info.taa.tp2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
@Entity
@NamedQueries({
                @NamedQuery(name="findUserStoryByPriority", query="SELECT us FROM UserStory us WHERE us.priority = :priority "),
                @NamedQuery(name="findUserStoryByEpic", query="SELECT us FROM UserStory us WHERE us.epic = :epic ")
        })
public class UserStory {
	
    // * Attributes * //
	
	private Long id;
    private String name;
    private String description;
    private int effortEstimate;
    private int priority;
    private Theme theme;
    private List<Task> tasks;
    private Epic epic;
    private StakeHolder stakeHolder;
    
    // * Constructors * //
    public UserStory() {
		this.name = new String();
		this.description = new String();
		this.effortEstimate = 0;
		this.priority = 0;
		tasks =  new ArrayList<Task>();
	}
    
    
    
    public UserStory(String name, String description, int effortEstimate,
			int priority, Theme theme, List<Task> tasks, StakeHolder stakeHolder) {
		super();
		this.name = name;
		this.description = description;
		this.effortEstimate = effortEstimate;
		this.priority = priority;
		this.theme = theme;
		
		for(Task t : tasks){
			t.setStory(this);
		}
		
		this.tasks = tasks;
		this.stakeHolder = stakeHolder;
		
		
	}

	public UserStory(String n) {
    	this.name = n;
    	this.description = new String();
		tasks =  new ArrayList<Task>();
	}
    
    
    /**
	 * @return the id
	 */    
	@Id
    @GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * @return the effortEstimate
	 */
	public int getEffortEstimate() {
		return effortEstimate;
	}

	/**
	 * @param effortEstimate the effortEstimate to set
	 */
	public void setEffortEstimate(int effortEstimate) {
		this.effortEstimate = effortEstimate;
	}

	/**
	 * @return the tasks
	 */
	@OneToMany(mappedBy="story",cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JsonIgnore
	public List<Task> getTasks() {
		return tasks;
	}

	/**
	 * @param tasks the tasks to set
	 */
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	/**
	 * @return the theme
	 */
	 @ManyToOne
	public Theme getTheme() {
		return theme;
	}

	/**
	 * @param theme the theme to set
	 */
	public void setTheme(Theme theme) {
		this.theme = theme;
	}


	@ManyToOne
    @JsonIgnore
	public Epic getEpic() {
		return epic;
	}



	public void setEpic(Epic epic) {
		this.epic = epic;
	}


	@ManyToOne
	public StakeHolder getStakeHolder() {
		return stakeHolder;
	}



	public void setStakeHolder(StakeHolder stakeHolder) {
		this.stakeHolder = stakeHolder;
	}


	@Override
	public String toString() {
		return "UserStory{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", effortEstimate=" + effortEstimate +
				", priority=" + priority +
				", theme=" + theme +
				", tasks=" + tasks +
				", epic=" + epic +
				", stakeHolder=" + stakeHolder +
				'}';
	}
}
