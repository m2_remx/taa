package fr.istic.m2info.taa.tp2.dao;

import fr.istic.m2info.taa.tp2.domain.Task;
import fr.istic.m2info.taa.tp2.domain.UserStory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */
public class ManagerTask {
	private EntityManager manager = ManagerSingletonConnection.getManagerConnection();;

	@Autowired
	ManagerUserStory managerUser;
	/**
	 *
	 * @param id
	 * @return
	 */
    public Task findById(long id) {
    	Task task = manager.find(Task.class, id);
        return task;
	}

	/**
	 *
	 * @return
	 */
    public List<Task> getAll() {
    	CriteriaBuilder cb = manager.getCriteriaBuilder();
    	 
		CriteriaQuery<Task> q = cb.createQuery(Task.class);
		Root<Task> c = q.from(Task.class);
		q.select(c);
		TypedQuery<Task> query = manager.createQuery(q);
		List<Task> result = query.getResultList();


        return result;
	}

	/**
	 *
	 * @param idUserStory
	 * @return
	 */
  /*  public List<Task> getByUserStory(Long idUserStory) {

		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery c = cb.createQuery(Task.class);
		Root acct = c.from(Task.class);
		c.select(acct).where(cb.equal(acct.get("story"),idUserStory));
		List result = manager.createQuery(c).getResultList();

        return result;
 	}
*/
	/**
	 *
	 * @param taskName
	 * @return
	 */
	public List<Task> getByName(String taskName){
		TypedQuery<Task> t = manager.createNamedQuery("findTaskByName", Task.class);
		t.setParameter("taskName", taskName+"%");
		List<Task> task = t.getResultList();
		return task;
	}

	/**
	 *
	 * @param userstoryId
	 * @return
     */
	public List<Task> getByUserStory(long userstoryId){
		TypedQuery<Task> t = manager.createNamedQuery("findTaskByUserStory", Task.class);
		System.out.println("Searching tasks... "+ userstoryId);
		UserStory e =managerUser.findById(userstoryId);
		if(e != null){
			t.setParameter("story", e);
			List<Task> tasks = t.getResultList();
			return tasks;
		}
		return new ArrayList<Task>();
	}

	/**
	 *
	 * @param task
	 * @return
	 */
	public boolean update(Task task){
		try {
			EntityTransaction et = manager.getTransaction();
			et.begin();

			Query q = manager.createQuery("UPDATE Task t SET t.name = :name , t.description = :description    WHERE t.id = :id");
			q.setParameter("name", task.getName());
			q.setParameter("description", task.getDescription());
			q.setParameter("id", task.getId());
			q.executeUpdate();

			et.commit();
		}catch(Exception e){
			System.out.println("updateTask " + e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 *
	 * @param task
	 * @return
	 */
	public boolean delete(Task task){
		EntityTransaction et = manager.getTransaction();
		try {
			et.begin();
			manager.remove(task);
			et.commit();
			return true;
		} catch (Exception e) {
			System.out.println("Delete TASK " + e.getMessage());
		}
		return false;
	}

	/**
	 *
	 * @param id
	 * @return
     */
	public boolean delete(Long id){
		try {
			Task task = findById(id);
			if(task!=null){
				return delete(task);
			}
		} catch (Exception e) {
			System.out.println("Delete TASK " + e.getMessage());
			e.printStackTrace();
		}

		return false;
	}

	/**
	 *
	 * @param task
	 * @return
	 */
	public boolean create(Task task){
		try {
			EntityTransaction et = manager.getTransaction();
			et.begin();
			UserStory us = managerUser.findById(task.getStory().getId());
			task.setStory(us);
			manager.persist(task);
			et.commit();
			return true;
		} catch (Exception e) {
			System.out.println("Create TASK " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

}
