package fr.istic.m2info.taa.tp2.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * We use le patron singleton for manager the BDD connection. We will create only
 * one connection.
 * @author  Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 */

public class ManagerSingletonConnection {

    //Only for the test
    //private static String CONNECTIONNAME = "dev";
    private static final String CONNECTIONNAME = "mysql_OK";
    private static EntityManager managerConnection;

    public static EntityManager getManagerConnection() {
        if (managerConnection == null || !managerConnection.isOpen()){
            System.out.println("Connection with : " + CONNECTIONNAME );
            EntityManagerFactory factory = Persistence.createEntityManagerFactory(CONNECTIONNAME);
            managerConnection = factory.createEntityManager();
        }
        return managerConnection;

    }

    public ManagerSingletonConnection() {
    }

    public static void closeConnection(){
        managerConnection.close();
    }

}
