package fr.istic.m2info.taa.tp2.rest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.logging.Logger;

/**
 * @author Mickaël CALLIMOUTOU / Mónica FERNÁNDEZ
 *
 */
//@Path("/api")
@RestController
@RequestMapping("/api")
public class SwaggerResource {

    private static final Logger logger = Logger.getLogger(SwaggerResource.class.getName());

    //@GET
    @RequestMapping(method = RequestMethod.GET )
    public byte[] Get1() {
        try {
            return Files.readAllBytes(FileSystems.getDefault().getPath("src/main/webapp/swagger/index.html"));
        } catch (IOException e) {
            return null;
        }
    }

    //@GET
    //@Path("{path:.*}")
    @RequestMapping(method = RequestMethod.GET , value = "{path:.*}")
    public byte[] Get(@PathVariable("path") String path) {
        try {
            return Files.readAllBytes(FileSystems.getDefault().getPath("src/main/webapp/swagger/"+path));
        } catch (IOException e) {
            return null;
        }
    }

}