'use strict';

myApp.controller('EpicController', ['$scope', 'EpicService', function($scope, EpicService) {
          var self = this;

          self.getAllEpic = function(){
			  EpicService.getAllEpic()
                  .then(
						  function(data) {
							  $scope.epics = data;
						  },
						  function(errResponse){
							  console.error('Error getAllEpic'+errResponse);
						  }
      			       );
          };


      }]);
