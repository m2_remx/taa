'use strict';

myApp.controller('UserStoryController', ['$rootScope','$scope', 'UserStoryService', function($rootScope,$scope, UserStoryService) {
    var self = this;

    self.getAllUserStory = function(){
        UserStoryService.getAllUserStory()
            .then(
                function(data) {
                    $scope.userstories = data;
                },
                function(errResponse){
                    console.error('Error getAllUserStory ' + errResponse);
                }
            );
    };

    self.getUserStoriesByEpic = function(){
        UserStoryService.getUserStoriesByEpic($scope.epicSelectedID)
            .then(
                function(data) {
                    $scope.userstories = data;
                },
                function(errResponse){
                    console.error('Error getUserStoriesByEpic ' + errResponse);
                }
            );
    };



    self.getAllEpic = function(){
        UserStoryService.getAllEpic()
            .then(
                function(data) {
                    $scope.epics = data;
                },
                function(errResponse){
                    console.error('Error getAllEpic: '+errResponse);
                }
            );
    };

    self.deleteEpic = function(){
        var idEpicToDelete = $scope.epicSelectedID ;
        if(idEpicToDelete != -1 && idEpicToDelete != null) {
            //alert(" Flag: Delete EpicController = "+idEpicToDelete+"?");
            UserStoryService.deleteEpic(idEpicToDelete)
                .then(
                    function() {
                        $scope.epicSelectedID = -1;
                        self.getAllEpic();
                    },
                    function(errResponse){
                        console.error('Error deleteEpic: '+errResponse);
                    }
                );
        }
    };

    self.addNewEpic = function(){
        $scope.epicSelectedID=-1;
        UserStoryService.addNewEpic($scope.newEpic)
            .then(
                function() {
                    document.getElementById("newepicwrapper").style.display = "none";
                    self.getAllEpic();
                },
                function(errResponse){
                    console.error('Error getAllEpic: '+errResponse);
                }
            );
    };

    self.addEpicView = function(){
        document.getElementById("newepicwrapper").style.display = "block";
        $scope.newEpic =  undefined;
    };

    self.selectUserStory = function(){

        //alert("Desde select user story"+$scope.userStoryidSelected);
        angular.forEach($scope.userstories, function (userstory, index) {
            if (userstory.id == $scope.userStoryidSelected ) {
                $scope.userStorySelected = userstory
            }
        });
        UserStoryService.getTasksByUserStory($scope.userStoryidSelected)
            .then(
                function(data) {
                    $scope.tasks = data;
                },
                function(errResponse){
                    console.error('Error getTasksByUserStory ' + errResponse);
                }
            );
    };

    self.addUserStoryView = function(){
        document.getElementById("newuserstorywrapper").style.display = "block";
        $scope.newUS = undefined;
    };

    self.addUserStory = function(){
        UserStoryService.addUserStory($scope.epicSelectedID, $scope.newUS)
            .then(
                function() {
                    $scope.userStoryidSelected=-1;
                    document.getElementById("newuserstorywrapper").style.display = "none";
                    if($scope.epicSelectedID != undefined && $scope.epicSelectedID!= -1){
                        self.getUserStoriesByEpic();
                    }else{
                        self.getAllUserStory();
                    }
                },
                function(errResponse){
                    console.error('Error addUserStory: '+errResponse);
                }
            );
        };

    self.deleteUserStory = function() {
        UserStoryService.deleteUserStory($scope.userStoryidSelected)
            .then(
                function() {
                    $scope.userStoryidSelected=-1;
                    if($scope.epicSelectedID != undefined && $scope.epicSelectedID!= -1){
                        self.getUserStoriesByEpic();
                    }else{
                        self.getAllUserStory();
                    }
                },
                function(errResponse){
                    console.error('Error deleteUserStory ' + errResponse);
                }
            );
    };

    //-- TASK --/
    self.addTaskView = function(){
        document.getElementById("newutaskwrapper").style.display ="block";
        $scope.newTask = undefined;
    };

    self.addTask = function(){
        UserStoryService.addTask($scope.userStoryidSelected, $scope.newTask)
            .then(
                function() {
                    document.getElementById("newutaskwrapper").style.display = "none";
                    UserStoryService.getTasksByUserStory($scope.userStoryidSelected)
                        .then(
                            function(data) {
                                $scope.tasks = data;
                            },
                            function(errResponse){
                                console.error('Error getTasksByUserStory ' + errResponse);
                            }
                        );
                },
                function(errResponse){
                    console.error('Error addTask: '+errResponse);
                }
            );
    };

    self.deleteTask = function(id) {
        UserStoryService.deleteTask(id)
            .then(
                function() {
                    UserStoryService.getTasksByUserStory($scope.userStoryidSelected)
                        .then(
                            function(data) {
                                $scope.tasks = data;
                            },
                            function(errResponse){
                                console.error('Error getTasksByUserStory ' + errResponse);
                            }
                        );
                },
                function(errResponse){
                    console.error('Error deleteTask ' + errResponse);
                }
            );
    };

    self.editTask= function(index) {
        $scope.selectedIndex=index;
        var liElement = document.getElementById("tasklist").getElementsByTagName("li")[index];
        var inputElements = liElement.getElementsByTagName("input");
        angular.forEach(inputElements, function (inputE, index) {
            inputE.removeAttribute("disabled");
        });


    };


    self.updateTask = function(t){
        alert(t.id);
        UserStoryService.updateTask(t)
            .then(
                function() {
                    $scope.selectedIndex=-1;
                },
                function(errResponse){
                    console.error('Error updateTask: '+errResponse);
                }
            );
    };

}]);
