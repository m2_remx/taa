'use strict';

myApp.factory('EpicService', ['$http', '$q', function($http, $q){
	return {

		getAllEpic: function() {
					return $http.get('http://localhost:8080/epic/all/')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching users');
										return $q.reject(errResponse);
									}
							);
			}
	};

}]);
