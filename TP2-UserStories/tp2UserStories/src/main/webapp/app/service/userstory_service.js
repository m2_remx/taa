'use strict';

myApp.factory('UserStoryService', ['$http', '$q', function($http, $q){
	return {
		getAllEpic: function() {
			return $http.get('http://localhost:8080/epic/all/')
					.then(
							function(response){
								return response.data;
							},
							function(errResponse){
								console.error('Error while fetching EPICS');
								return $q.reject(errResponse);
							}
					);
		},

		deleteEpic: function(id) {
			//alert("Flag: Delete EpicService : ID = "+id+"?");
			return $http.delete('http://localhost:8080/epic/'+id)
					.then(
							function(){
								alert('Epic deleted');
							},
							function(errResponse){
								console.error('Error Delete EPIC');
								return $q.reject(errResponse);
							}
					);
		},

		addNewEpic: function(epic) {
			var req = {
				method: 'POST',
				url: 'http://localhost:8080/epic/',
				headers: {
					'Content-Type': 'application/json'
				},
				data: { name: epic.name,
						description : epic.descr
				}
			}

			return $http(req)
					.then(
							function(){
								return;
							},
							function(errResponse){
								console.error('Error Delete EPIC');
								return $q.reject(errResponse);
							}
					);
		},

		getAllUserStory: function() {
			return $http.get('http://localhost:8080/userstory/all/')
				.then(
					function(response){
						return response.data;
					},
					function(errResponse){
						console.error('getAllUserStory Error while fetching users');
						return $q.reject(errResponse);
					}
				);
		},

		getUserStoriesByEpic: function(idEpic) {
			return $http.get('http://localhost:8080/userstory/epic/'+idEpic)
					.then(
							function(response){
								return response.data;
							},
							function(errResponse){
								console.error('getUserStoriesByEpic Error while fetching users');
								return $q.reject(errResponse);
							}
					);
		},





		// ------ USER STORY --- //
		addUserStory: function(idEpic,us) {
			var req = {
				method: 'POST',
				url: 'http://localhost:8080/userstory/',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					name: us.name,
					description : us.descr,
					epic:{
						id : idEpic
					} ,
					stakeHolder: {
						name : us.stakeholder
					}
				}
			}

			return $http(req)
					.then(
							function(){
								return;
							},
							function(errResponse){
								console.error('Error addUserStory');
								return $q.reject(errResponse);
							}
					);
		},

		deleteUserStory: function(id) {
			return $http.delete('http://localhost:8080/userstory/'+id)
					.then(
							function(){
								return;
							},
							function(errResponse){
								console.error('Error while fetching users');
								return $q.reject(errResponse);
							}
					);
		},
		// ------ TASK --- //

		getTasksByUserStory: function(idUs) {
			return $http.get('http://localhost:8080/task/userstory/'+idUs)
					.then(
							function(response){
								return response.data;
							},
							function(errResponse){
								console.error('getTasksByUserStory Error while fetching users');
								return $q.reject(errResponse);
							}
					);
		},


		addTask: function(idUS, task) {
			var req = {
				method: 'POST',
				url: 'http://localhost:8080/task/',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					name: task.name,
					description : task.descr,
					story:{
						id : idUS
					}
				}
			}

			return $http(req)
					.then(
							function(){
								return;
							},
							function(errResponse){
								console.error('Error addTask');
								return $q.reject(errResponse);
							}
					);
		},

		deleteTask: function(id) {
			return $http.delete('http://localhost:8080/task/'+id)
					.then(
							function(){
								return;
							},
							function(errResponse){
								console.error('Error while fetching users');
								return $q.reject(errResponse);
							}
					);
		},

		updateTask: function(task) {
			var req = {
				method: 'POST',
				url: 'http://localhost:8080/task/update',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					id:task.id,
					name:task.name,
					description: task.description
				}
			}

			return $http(req)
					.then(
							function(){
								return;
							},
							function(errResponse){
								console.error('Error updateTask');
								return $q.reject(errResponse);
							}
					);
		},

	};


}]);
